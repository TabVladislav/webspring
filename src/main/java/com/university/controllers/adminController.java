package com.university.controllers;

import com.university.service.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ADMIN')")
public class adminController {
    private final UserServiceImpl userService;

    @GetMapping("/admin")
    public String Users(Model model) {
        model.addAttribute("users", userService.list());
        return "admin";
    }
}