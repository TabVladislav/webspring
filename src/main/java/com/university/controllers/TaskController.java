package com.university.controllers;

import com.university.domain.entity.Task;
import com.university.service.TaskServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class TaskController {

    private final TaskServiceImpl taskService;


    @GetMapping("/")
    public String tasks(@RequestParam(name = "title", required = false) String title, Principal principal, Model model) {
        model.addAttribute("tasks", taskService.listProducts(title));
        model.addAttribute("user", taskService.getUserByPrincipal(principal));
        return "index";
    }

    @PostMapping("/task/create")
    public String createTask(Task task, Principal principal) throws IOException {
        taskService.createTask(task, principal);
        return "redirect:/";
    }

    @PostMapping("/task/delete")
    public String deleteAllTasks() {
        taskService.deleteAll();
        return "redirect:/";
    }
}
